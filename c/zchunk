/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Lib/SparkLib/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.chunk */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "zflex.h"
#include "err.h"
#include "sfs.h"
#include "zbuffer.h"
#include "convert.h"
#include "arcs.h"
#include "sarcfs.h"
#include "zchunk.h"


/*****************************************************************************/



/* shift 'size' bytes from offset src to offset dest */

_kernel_oserror * movechunk(archive * arc,int dest,int src,int size)
{
 _kernel_oserror * err;
 char            * buff;
 int               bsize;
 int               left;
 int               sp;
 int               dp;
 int               rlen;

 err=NULL;

 deb("move chunk dets=%d src=%d size=%d",dest,src,size);


 if(size && dest!=src)
 {
  err=buff_alloc((flex_ptr)&buff,size,&bsize);

  if(!err)
  {
   left=size;

   if(dest>src)    /* moving stuff up the file */
   {
    sp=src+size;
    dp=dest+size;

    while(left)
    {
     if(left<bsize) rlen=left;
     else           rlen=bsize;

     sp-=rlen;
     dp-=rlen;

     err=seekarc(arc,sp);
     if(err) break;

     err=readarc(arc,buff,rlen);
     if(err) break;

     err=seekarc(arc,dp);
     if(err) break;

     err=writearc(arc,buff,rlen);
     if(err) break;

     left-=rlen;
    }
   }
   else
   if(dest<src)    /* moving stuff down the file - compacting it */
   {
    sp=src;
    dp=dest;

    while(left)
    {
     if(left<bsize) rlen=left;
     else           rlen=bsize;

     err=seekarc(arc,sp);
     if(err) break;

     err=readarc(arc,buff,rlen);
     if(err) break;

     err=seekarc(arc,dp);
     if(err) break;

     err=writearc(arc,buff,rlen);
     if(err) break;

     left-=rlen;
     sp+=rlen;
     dp+=rlen;
    }
   }

   flex_free((flex_ptr)&buff);
  }
 }
 return(err);
}



